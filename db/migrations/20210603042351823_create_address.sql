-- +micrate Up
CREATE TABLE addresses (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  business_id BIGINT,
  street VARCHAR(100),
  neighborhood VARCHAR(100),
  zip VARCHAR(10),
  city VARCHAR(100),
  state VARCHAR(100),
  country VARCHAR(50),
  phone VARCHAR(10),
  email VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT `fk_business_address`
    FOREIGN KEY (business_id) REFERENCES businesses (id)
      ON UPDATE Restrict
      ON DELETE CASCADE
);
CREATE INDEX address_business_id_idx ON addresses (business_id);

-- +micrate Down
DROP TABLE IF EXISTS addresses;
