-- +micrate Up
CREATE TABLE businesses (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id BIGINT,
  name VARCHAR(255),
  description TEXT,
  email VARCHAR(255),
  tags VARCHAR(255),
  domain VARCHAR(255),
  slug VARCHAR(50),
  facebook VARCHAR(50),
  instagram VARCHAR(50),
  template VARCHAR(50),
  active BOOL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT `fk_user_business`
    FOREIGN KEY (user_id) REFERENCES users (id)
      ON UPDATE RESTRICT
      ON DELETE CASCADE
);
CREATE INDEX business_name_idx ON businesses (name);
CREATE INDEX business_slug_idx ON businesses (slug);
CREATE INDEX business_tags_idx ON businesses (tags);
CREATE INDEX business_user_id_idx ON businesses (user_id);

-- +micrate Down
DROP TABLE IF EXISTS businesses;
