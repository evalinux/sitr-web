-- +micrate Up
CREATE TABLE media (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  business_id BIGINT,
  host VARCHAR(50),
  url VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT `fk_business_media`
    FOREIGN KEY (business_id) REFERENCES businesses (id)
      ON UPDATE RESTRICT
      ON DELETE CASCADE
);
CREATE INDEX media_business_id_idx ON media (business_id);

-- +micrate Down
DROP TABLE IF EXISTS media;
