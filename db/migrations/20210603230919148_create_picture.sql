-- +micrate Up
CREATE TABLE pictures (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  business_id BIGINT,
  image LONGTEXT,
  mimetype VARCHAR(50),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT `fk_business_picture`
    FOREIGN KEY (business_id) REFERENCES businesses (id)
      ON UPDATE Restrict
      ON DELETE CASCADE
);
CREATE INDEX picture_business_id_idx ON pictures (business_id);

-- +micrate Down
DROP TABLE IF EXISTS pictures;
