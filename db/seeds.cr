require "../config/application"
require "faker"

# settings
number_of_users = 0
number_of_businesses = 0
number_of_addresses = 0


# users
User.create(name: "Renich Bon Ćirić", email: "renich@evalinux.com", mobile: "3335765013")
User.create(name: "Aldo Romero", email: "aldo@lavaldo.com", mobile: "3312345678")
User.create(name: "Yann Kostic", email: "yann@ouioui.com", mobile: "3312454698")

1.times do
  models = [] of User
  number_of_users.times do
    models << User.new(name: Faker::Name.name, email: Faker::Internet.email, mobile: Faker::Number.number(10))
  end

  User.import(models)
end


# businesses
## EVALinux
b = Business.new
b.user_id = 1
b.name = "EVALinux"
b.description = "DevOps and Systems Administration. We reduce costs in physical or virtual infrastructure."
b.email = "contact@evalinux.com"
b.tags = "servers, virtualization, websites, web, www, nginx, php, ruby, crystal, sysadmin, systems administration, devops, development"
b.slug = "evalinux"
b.template = "1"
b.facebook = "evalinux"
b.instagram = "eva.linux"
b.active = true
b.save

## LavAldo
b = Business.new
b.user_id = 2
b.name = "Lavaldo, S.A. de C.V."
b.description = "The best washing service all around. We can help you get your close clean in no time!"
b.email = "contact@lavaldo.com.mx"
b.tags = "washing, ironing, cleaning, clean-up, cleanliness, express washing, 24/7"
b.domain = "lavaldo.com.mx"
b.slug = "lavaldo"
b.template = "2"
b.facebook = "lavaldo"
b.instagram = "lava.aldo"
b.active = true
b.save

## Oui Oui
b = Business.new
b.user_id = 3
b.name = "Oui Oui"
b.description = "Un lugar hermoso lleno de magia, deliciosos platillos, música en vivo de clase mundial y un ambiente romántico, de clase, único y exclusivo."
b.email = "gabymdesantiago@gmail.com"
b.tags = "french restaurant, french cousine, drinks, live music, ajijic, chapala lake"
b.slug = "ouioui"
b.template = "1"
b.facebook = "leclub4ouioui"
b.instagram = "leclub4ouioui"
b.active = true
b.save

## Fakes
1.times do
  models = [] of Business
  number_of_businesses.times do
    models << Business.new(
      user_id: Faker::Number.between(4, 100).to_i64,
      name: Faker::Company.name,
      description: Faker::Lorem.sentence(2),
      email: Faker::Internet.free_email,
      tags: Faker::Lorem.words(5).join(", "),
      domain: nil,
      slug: Faker::Internet.slug,
      template: Faker::Number.between(1, 4).to_s,
      facebook: Faker::Internet.slug,
      instagram: Faker::Internet.slug,
      active: true
    )
  end

  Business.import(models)
end


# addresses
## EVALinux
a = Address.new
a.business_id = 1
a.street = "Jusé Antonio Torres 72b"
a.neighborhood = "Ojo de Agua"
a.zip = "45850"
a.city = "Ixtlahuacán de los Membrillos"
a.state = "Jalisco"
a.country = "México"
a.phone = "3767620811"
a.email = "contact@evalinux.com"
a.save

## LavAldo
a = Address.new
a.business_id = 2
a.street = "Zaragoza 24"
a.neighborhood = "Ojo de Agua"
a.zip = "45850"
a.city = "Ixtlahuacán de los Membrillos"
a.state = "Jalisco"
a.country = "México"
a.phone = "3761234567"
a.email = "ixtlahuacan@lavaldo.com.mx"
a.save

a = Address.new
a.business_id = 2
a.street = "Pedro Moreno 38"
a.neighborhood = "Centro"
a.zip = "45900"
a.city = "Chapala"
a.state = "Jalisco"
a.country = "México"
a.phone = "3763217654"
a.email = "chapala@lavaldo.com.mx"
a.save

## Oui Oui
a = Address.new
a.business_id = 3
a.street = "Independencia 2"
a.neighborhood = "Centro"
a.zip = "45920"
a.city = "Ajijic"
a.state = "Jalisco"
a.country = "México"
a.phone = "3767661360"
a.email = "gabymdesantiago@gmail.com"
a.save

## Fakes
1.times do
  models = [] of Address
  number_of_addresses.times do
    models << Address.new(
      business_id: Faker::Number.between(4, 200).to_i64,
      street: Faker::Address.street_name,
      neighborhood: Faker::Address.street_name,
      zip: Faker::Address.postcode.to_s,
      city: Faker::Address.city,
      state: "Jalisco",
      country: "México",
      phone: Faker::Number.number(10),
      email: Faker::Internet.free_email,
    )
  end

  Address.import(models)
end

