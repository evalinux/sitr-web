require "./spec_helper"

def address_hash
  {"business_id" => "1", "street" => "Fake", "neighborhood" => "Fake", "zip" => "Fake", "city" => "Fake", "state" => "Fake", "country" => "Fake", "phone" => "Fake", "email" => "Fake"}
end

def address_params
  params = [] of String
  params << "business_id=#{address_hash["business_id"]}"
  params << "street=#{address_hash["street"]}"
  params << "neighborhood=#{address_hash["neighborhood"]}"
  params << "zip=#{address_hash["zip"]}"
  params << "city=#{address_hash["city"]}"
  params << "state=#{address_hash["state"]}"
  params << "country=#{address_hash["country"]}"
  params << "phone=#{address_hash["phone"]}"
  params << "email=#{address_hash["email"]}"
  params.join("&")
end

def create_address
  model = Address.new(address_hash)
  model.save
  model
end

class AddressControllerTest < GarnetSpec::SystemTest
  getter handler : Amber::Pipe::Pipeline

  def initialize
    @handler = Amber::Pipe::Pipeline.new
    @handler.build :web do
      plug Amber::Pipe::Error.new
      plug Amber::Pipe::Session.new
      plug Amber::Pipe::Flash.new
    end
    @handler.prepare_pipelines
  end
end

describe AddressControllerTest do
  subject = AddressControllerTest.new

  it "renders address index template" do
    Address.clear
    response = subject.get "/addresses"

    response.status_code.should eq(200)
    response.body.should contain("addresses")
  end

  it "renders address show template" do
    Address.clear
    model = create_address
    location = "/addresses/#{model.id}"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Show Address")
  end

  it "renders address new template" do
    Address.clear
    location = "/addresses/new"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("New Address")
  end

  it "renders address edit template" do
    Address.clear
    model = create_address
    location = "/addresses/#{model.id}/edit"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Edit Address")
  end

  it "creates a address" do
    Address.clear
    response = subject.post "/addresses", body: address_params

    response.headers["Location"].should eq "/addresses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "updates a address" do
    Address.clear
    model = create_address
    response = subject.patch "/addresses/#{model.id}", body: address_params

    response.headers["Location"].should eq "/addresses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "deletes a address" do
    Address.clear
    model = create_address
    response = subject.delete "/addresses/#{model.id}"

    response.headers["Location"].should eq "/addresses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end
end
