require "./spec_helper"

def media_hash
  {"business_id" => "1", "host" => "Fake", "url" => "Fake"}
end

def media_params
  params = [] of String
  params << "business_id=#{media_hash["business_id"]}"
  params << "host=#{media_hash["host"]}"
  params << "url=#{media_hash["url"]}"
  params.join("&")
end

def create_media
  model = Media.new(media_hash)
  model.save
  model
end

class MediaControllerTest < GarnetSpec::SystemTest
  getter handler : Amber::Pipe::Pipeline

  def initialize
    @handler = Amber::Pipe::Pipeline.new
    @handler.build :web do
      plug Amber::Pipe::Error.new
      plug Amber::Pipe::Session.new
      plug Amber::Pipe::Flash.new
    end
    @handler.prepare_pipelines
  end
end

describe MediaControllerTest do
  subject = MediaControllerTest.new

  it "renders media index template" do
    Media.clear
    response = subject.get "/media"

    response.status_code.should eq(200)
    response.body.should contain("media")
  end

  it "renders media show template" do
    Media.clear
    model = create_media
    location = "/media/#{model.id}"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Show Media")
  end

  it "renders media new template" do
    Media.clear
    location = "/media/new"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("New Media")
  end

  it "renders media edit template" do
    Media.clear
    model = create_media
    location = "/media/#{model.id}/edit"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Edit Media")
  end

  it "creates a media" do
    Media.clear
    response = subject.post "/media", body: media_params

    response.headers["Location"].should eq "/media"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "updates a media" do
    Media.clear
    model = create_media
    response = subject.patch "/media/#{model.id}", body: media_params

    response.headers["Location"].should eq "/media"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "deletes a media" do
    Media.clear
    model = create_media
    response = subject.delete "/media/#{model.id}"

    response.headers["Location"].should eq "/media"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end
end
