require "./spec_helper"

def business_hash
  {"user_id" => "1", "name" => "Fake", "email" => "Fake", "tags" => "Fake", "domain" => "Fake", "slug" => "Fake", "facebook" => "Fake", "instagram" => "Fake", "active" => "1"}
end

def business_params
  params = [] of String
  params << "user_id=#{business_hash["user_id"]}"
  params << "name=#{business_hash["name"]}"
  params << "email=#{business_hash["email"]}"
  params << "tags=#{business_hash["tags"]}"
  params << "domain=#{business_hash["domain"]}"
  params << "slug=#{business_hash["slug"]}"
  params << "facebook=#{business_hash["facebook"]}"
  params << "instagram=#{business_hash["instagram"]}"
  params << "active=#{business_hash["active"]}"
  params.join("&")
end

def create_business
  model = Business.new(business_hash)
  model.save
  model
end

class BusinessControllerTest < GarnetSpec::SystemTest
  getter handler : Amber::Pipe::Pipeline

  def initialize
    @handler = Amber::Pipe::Pipeline.new
    @handler.build :web do
      plug Amber::Pipe::Error.new
      plug Amber::Pipe::Session.new
      plug Amber::Pipe::Flash.new
    end
    @handler.prepare_pipelines
  end
end

describe BusinessControllerTest do
  subject = BusinessControllerTest.new

  it "renders business index template" do
    Business.clear
    response = subject.get "/businesses"

    response.status_code.should eq(200)
    response.body.should contain("businesses")
  end

  it "renders business show template" do
    Business.clear
    model = create_business
    location = "/businesses/#{model.id}"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Show Business")
  end

  it "renders business new template" do
    Business.clear
    location = "/businesses/new"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("New Business")
  end

  it "renders business edit template" do
    Business.clear
    model = create_business
    location = "/businesses/#{model.id}/edit"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Edit Business")
  end

  it "creates a business" do
    Business.clear
    response = subject.post "/businesses", body: business_params

    response.headers["Location"].should eq "/businesses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "updates a business" do
    Business.clear
    model = create_business
    response = subject.patch "/businesses/#{model.id}", body: business_params

    response.headers["Location"].should eq "/businesses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "deletes a business" do
    Business.clear
    model = create_business
    response = subject.delete "/businesses/#{model.id}"

    response.headers["Location"].should eq "/businesses"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end
end
