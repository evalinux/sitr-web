require "./spec_helper"

def picture_hash
  {"business_id" => "1", "image" => "Fake", "mimetype" => "Fake"}
end

def picture_params
  params = [] of String
  params << "business_id=#{picture_hash["business_id"]}"
  params << "image=#{picture_hash["image"]}"
  params << "mimetype=#{picture_hash["mimetype"]}"
  params.join("&")
end

def create_picture
  model = Picture.new(picture_hash)
  model.save
  model
end

class PictureControllerTest < GarnetSpec::SystemTest
  getter handler : Amber::Pipe::Pipeline

  def initialize
    @handler = Amber::Pipe::Pipeline.new
    @handler.build :web do
      plug Amber::Pipe::Error.new
      plug Amber::Pipe::Session.new
      plug Amber::Pipe::Flash.new
    end
    @handler.prepare_pipelines
  end
end

describe PictureControllerTest do
  subject = PictureControllerTest.new

  it "renders picture index template" do
    Picture.clear
    response = subject.get "/pictures"

    response.status_code.should eq(200)
    response.body.should contain("pictures")
  end

  it "renders picture show template" do
    Picture.clear
    model = create_picture
    location = "/pictures/#{model.id}"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Show Picture")
  end

  it "renders picture new template" do
    Picture.clear
    location = "/pictures/new"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("New Picture")
  end

  it "renders picture edit template" do
    Picture.clear
    model = create_picture
    location = "/pictures/#{model.id}/edit"

    response = subject.get location

    response.status_code.should eq(200)
    response.body.should contain("Edit Picture")
  end

  it "creates a picture" do
    Picture.clear
    response = subject.post "/pictures", body: picture_params

    response.headers["Location"].should eq "/pictures"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "updates a picture" do
    Picture.clear
    model = create_picture
    response = subject.patch "/pictures/#{model.id}", body: picture_params

    response.headers["Location"].should eq "/pictures"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end

  it "deletes a picture" do
    Picture.clear
    model = create_picture
    response = subject.delete "/pictures/#{model.id}"

    response.headers["Location"].should eq "/pictures"
    response.status_code.should eq(302)
    response.body.should eq "302"
  end
end
