class BusinessController < ApplicationController
  getter business = Business.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_business }
  end

  def index
    businesses = Business.all("JOIN users u ON u.id = user_id ORDER BY businesses.name")
    render "index.slang"
  end

  def show
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def edit
    render "edit.slang"
  end

  def create
    business = Business.new business_params.validate!
    if business.save
      redirect_to action: :index, flash: {"success" => "Business has been created."}
    else
      flash[:danger] = "Could not create Business!"
      render "new.slang"
    end
  end

  def update
    business.set_attributes business_params.validate!
    if business.save
      redirect_to action: :index, flash: {"success" => "Business has been updated."}
    else
      flash[:danger] = "Could not update Business!"
      render "edit.slang"
    end
  end

  def destroy
    business.destroy
    redirect_to action: :index, flash: {"success" => "Business has been deleted."}
  end

  private def business_params
    params.validation do
      required :user_id
      required :name
      required :description
      optional :email
      required :tags
      optional :domain
      required :slug
      required :template
      optional :facebook
      optional :instagram
      optional :active
    end
  end

  private def set_business
    @business = Business.find! params[:id]
  end
end
