class MediaController < ApplicationController
  getter media = Media.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_media }
  end

  def index
    media = Media.all
    render "index.slang"
  end

  def show
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def edit
    render "edit.slang"
  end

  def create
    media = Media.new media_params.validate!
    if media.save
      redirect_to action: :index, flash: {"success" => "Media has been created."}
    else
      flash[:danger] = "Could not create Media!"
      render "new.slang"
    end
  end

  def update
    media.set_attributes media_params.validate!
    if media.save
      redirect_to action: :index, flash: {"success" => "Media has been updated."}
    else
      flash[:danger] = "Could not update Media!"
      render "edit.slang"
    end
  end

  def destroy
    media.destroy
    redirect_to action: :index, flash: {"success" => "Media has been deleted."}
  end

  private def media_params
    params.validation do
      required :business_id
      required :host
      required :url
    end
  end

  private def set_media
    @media = Media.find! params[:id]
  end
end
