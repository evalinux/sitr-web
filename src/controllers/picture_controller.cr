require "base64"

class PictureController < ApplicationController
  getter picture = Picture.new
  getter business = Business.new

  before_action do
    only [:destroy] { set_picture }
    only [:show] { set_business }
  end

  def index
    businesses = Business.order(name: :asc)
    render "index.slang"
  end

  def edit
  end

  def show
    pictures = Picture.all("JOIN businesses b ON b.id = business_id ORDER BY b.name, pictures.created_at")
    pictures = Picture.all("
      JOIN businesses b ON b.id = business_id
      WHERE b.id = #{business.id}
      ORDER BY b.name, pictures.created_at
    ")
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def create
    # get picture
    image = params.files["picture"]
    name = image.filename
    file = Base64.strict_encode(image.file.gets_to_end)
    type = image.headers["content-Type"]

    # validate
    picture_params.validate!

    # assign
    picture = Picture.new
    picture.business_id = picture_params[:business_id].to_i64
    picture.image = file
    picture.mimetype = type

    if picture.save
      redirect_to action: :index, flash: {"success" => "Picture has been created."}
    else
      flash[:danger] = "Could not create Picture!"
      render "new.slang"
    end
  end

  def update
  end

  def destroy
    picture.destroy
    redirect_to action: :index, flash: {"success" => "Picture has been deleted."}
  end

  private def picture_params
    params.validation do
      required :business_id
      optional :image
    end
  end

  private def set_business
    @business = Business.find! params[:id]
  end

  private def set_picture
    @picture = Picture.find! params[:id]
  end
end
