class AddressController < ApplicationController
  getter address = Address.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_address }
  end

  def index
    addresses = Address.all("JOIN businesses b ON b.id = business_id ORDER BY b.name, city, street")
    render "index.slang"
  end

  def show
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def edit
    render "edit.slang"
  end

  def create
    address = Address.new address_params.validate!
    if address.save
      redirect_to action: :index, flash: {"success" => "Address has been created."}
    else
      flash[:danger] = "Could not create Address!"
      render "new.slang"
    end
  end

  def update
    address.set_attributes address_params.validate!
    if address.save
      redirect_to action: :index, flash: {"success" => "Address has been updated."}
    else
      flash[:danger] = "Could not update Address!"
      render "edit.slang"
    end
  end

  def destroy
    address.destroy
    redirect_to action: :index, flash: {"success" => "Address has been deleted."}
  end

  private def address_params
    params.validation do
      required :business_id
      required :street
      required :neighborhood
      required :zip
      required :city
      required :state
      required :country
      optional :phone { |p| p.phone?(locale = "en-US") }
      optional :email { |e| e.email? }
    end
  end

  private def set_address
    @address = Address.find! params[:id]
  end
end
