class Picture < Granite::Base
  connection mysql
  table pictures

  belongs_to :business

  column id : Int64, primary: true
  column image : String?
  column mimetype : String?
end
