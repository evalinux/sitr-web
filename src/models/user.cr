class User < Granite::Base
  connection mysql
  table users

  has_many :business

  column id : Int64, primary: true
  column name : String?
  column email : String?
  column mobile : String?
end
