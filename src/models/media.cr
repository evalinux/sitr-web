class Media < Granite::Base
  connection mysql
  table media

  belongs_to :business

  column id : Int64, primary: true
  column host : String?
  column url : String?
end
