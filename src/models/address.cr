class Address < Granite::Base
  connection mysql
  table addresses

  belongs_to :business

  column id : Int64, primary: true
  column street : String?
  column neighborhood : String?
  column zip : String?
  column city : String?
  column state : String?
  column country : String?
  column phone : String?
  column email : String?
end
