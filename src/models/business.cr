class Business < Granite::Base
  connection mysql
  table businesses

  belongs_to :user

  has_many :address
  has_many :media
  has_many :picture

  column id : Int64, primary: true
  column name : String?
  column description : String?
  column email : String?
  column tags : String?
  column domain : String?
  column slug : String?
  column template : String?
  column facebook : String?
  column instagram : String?
  column active : Bool?
end
